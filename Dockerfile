FROM python:3.9.15-slim-bullseye
WORKDIR /repo
COPY main.py ./
RUN pip install datetime
RUN pip install numpy
RUN pip install pandas
RUN pip install pymysql
RUN pip install python-dotenv
CMD ["python","./main.py"]

