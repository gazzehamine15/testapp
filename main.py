from urllib.request import urlopen
from datetime import datetime
import numpy as np
import pymysql
import pandas as pd
import json


# Récupération des données par api
url = "https://opendata.paris.fr/api/records/1.0/search/?dataset=les-arbres-plantes&q=&rows=3006"
response = urlopen(url)
data_json = json.loads(response.read())

# connexion à la base des données
con = pymysql.connect(user="sql12573965", password="v3AGguqp8c", host="sql12.freemysqlhosting.net",
                      port=3306, database="sql12573965")
cursor = con.cursor()

# Convertir le fichier JSON en data frame
df = pd.json_normalize(data_json['records'])
agearbreenjours = []
vitessecroissance = []
dateplantation = []

# Transformation des données
for i in range(len(df)):

    # Création d'une liste qui contient l'age de chaque arbre en jours
    agearbreenjours.append(int(str(datetime.today()
                               - datetime.strptime(
        df['fields.dateplantation'][i][0:10], "%Y-%m-%d")).split(' ')[0]))

    # Création d'une liste qui contient la vitesse de croissance de chaque arbre
    vitessecroissance.append(df['fields.hauteurenm'][i]/int(agearbreenjours[i])
                             )

    # Modification de la date de plantation pour qu'elle soit une date valide
    dateplantation.append(
        df['fields.dateplantation'][i][0:10])

# Création des nouvelles colonnes et insertion des données.
df['fields.dateplantation'] = dateplantation
df['fields.agearbreenjours'] = agearbreenjours
df['fields.vitessecroissance'] = vitessecroissance

df = df.replace(np.nan, 'None')

# Requete SQL pour la création d'une table arbre dans la base des données
createTableStmt = "CREATE TABLE arbre(idbase INT PRIMARY KEY, adresse VARCHAR(250), arrondissement VARCHAR(100), espece VARCHAR(100), genre VARCHAR(100), libellefrancais VARCHAR(100), varieteoucultivar VARCHAR(100), dateplantation DATE,agearbreenjours INT,vitessecroissance DOUBLE, hauteurenm DOUBLE, circonferenceencm DOUBLE, longitude DOUBLE, latitude DOUBLE)"

# Requete SQL pour l'insertion des données dans la table arbre
insertStmt = "INSERT INTO `arbre` (`idbase`, `adresse`, `arrondissement`, `espece`,`genre`,`libellefrancais`,`varieteoucultivar`, `dateplantation`,`agearbreenjours`,`vitessecroissance`, `hauteurenm`, `circonferenceencm`,`longitude`,`latitude`) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s ,%s, %s, %s, %s, %s)"

# Requete SQL pour vérifier l'existence de la table arbre dans la base des données
testStmt = "SHOW TABLES LIKE 'arbre'"
cursor.execute(testStmt)

# Si la table arbre existe déjà, on va l'effacer
result = cursor.fetchone()
if result:
    dropStmt = "DROP TABLE arbre"
    cursor.execute(dropStmt)

# Exécution de la requete de création de la table arbre
cursor.execute(createTableStmt)

# Exécution de la requete d'insertion des données
for i in range(len(df)):
    cursor.execute(insertStmt, (df['fields.idbase'][i], df['fields.adresse'][i], df['fields.arrondissement'][i], df['fields.espece'][i], df['fields.genre'][i], df['fields.libellefrancais'][i],
                                df['fields.varieteoucultivar'][i], df['fields.dateplantation'][i], df['fields.agearbreenjours'][i], df['fields.vitessecroissance'][i], df['fields.hauteurenm'][i], df['fields.circonferenceencm'][i], df['geometry.coordinates'][i][0], df['geometry.coordinates'][i][1]))
con.commit()
